// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery-ui/effect-transfer
//= require jquery-ui/effect-highlight
//= require jquery_ujs
//= require_tree .
//= require foundation
//= require tinymce-jquery


$(function(){ $(document).foundation({})});

// Если что то не работает при использовании data-turbolinks, 
// то сдесь переинициализируем
$(document).ready(function () {
  $(document).foundation('topbar', 'reflow');
  $(document).foundation('abide', 'reflow');
//  $("h1").html("fetch!")
});

//= require turbolinks