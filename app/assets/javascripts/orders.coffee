# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  

  init = () ->
    myMap = null
    myPlacemark = null
    $("input:radio").change ->
      

      if (myMap)
        myMap.destroy()
        myMap = null
        #myPlacemark.destroy()
        myPlacemark = null

      if $(@).val()<100
        $('.map').removeClass('hidden')
        magazine_id = $(@).val()
        switch (magazine_id)
          when "1"  
            myMap = new ymaps.Map("map", {
              center: [59.919581, 30.465186],
              zoom: 15
              }, {autoFitToViewport: "always"})
            myPlacemark = new ymaps.Placemark([59.919581, 30.465186], {
              hintContent: "проспект Пятилеток, 2",
              balloonContent: "магазин «Косметичка-СПб» на Большевиков"
            })
          when "2" 
            myMap = new ymaps.Map("map", {
              center: [59.946175, 30.473981],
              zoom: 15
              }, {autoFitToViewport: "always"})
            myPlacemark = new ymaps.Placemark([59.946175, 30.473981], {
              hintContent: "ТЦ «Июнь», Индустриальный проспект, 24",
              balloonContent: "магазин «Косметичка-СПб» на Большевиков"
            })      
        myMap.geoObjects.add(myPlacemark)
      else
        $('.map').addClass('hidden')
    return

  ymaps.ready(init)

  return