class ApplicationController < ActionController::Base
  before_action :set_main_cat
  before_action :set_locale
  before_action :authorize

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

 
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  protected


    def authorize
      unless User.find_by(id: session[:user_id])
        redirect_to login_url, notice: "Пожалуйста, пройдите авторизацию"
      end
    end


    # Проверяет наличие главной категории, если её нет, то создаёт её
    def set_main_cat
      main_cat = Category.find(1)
      main_cat.update( parent_id: 0) if main_cat.parent_id != 0
      main_cat.update( title: "Главная") if main_cat.title != "Главная"
    rescue ActiveRecord::RecordNotFound
      Category.create id: 1, title: "Главная", parent_id: 0
    end

end
