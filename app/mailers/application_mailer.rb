class ApplicationMailer < ActionMailer::Base
  default from: "mailer@kosmetichka-spb.ru"
  layout 'mailer'
end
