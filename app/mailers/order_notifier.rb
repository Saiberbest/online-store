class OrderNotifier < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.received.subject
  #
  def received(order)
    @order = order
    mail to: order.email, subject: 'Подтверждение заказа на kosmetichka-spb.ru'    
  end

  def received_admin(order)
    @order = order
    mail to: "saiberbest@gmail.com", subject: 'Новый заказ на kosmetichka-spb.ru'
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.shipped.subject
  #
  def shipped(order)
    @order = order

    mail to: order.email, subject: 'Ваш заказ на kosmetichka-spb.ru отправлен'
  end
end
