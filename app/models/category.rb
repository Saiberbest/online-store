class Category < ActiveRecord::Base
  has_many :subcategories, class_name: "Category", foreign_key: "parent_id"
  belongs_to :parent, class_name: "Category"
  has_many :products

  before_destroy :ensure_not_referenced_by_any_product
  before_destroy :ensure_not_referenced_by_any_category


  validates :title, :parent_id, presence: true
  validate :check_parent_id
  validates :parent_id, inclusion: {in: Category.all.map(&:id), message: "Выбранной родительской категории не существует."}

  def get_all_subcats_ids
    subcategories_ids=[]
    subcategories.each do |subcat|
      subcategories_ids << subcat.id 
      subcategories_ids.concat(subcat.get_all_subcats_ids)
    end
    subcategories_ids
  end

  def get_all_subcats
    Category.where(id: [get_all_subcats_ids()])
  end

  def get_all_products
    Product.where(category_id: [get_all_subcats_ids(), id])
  end

  def get_all_parents
    all_parents = []
    if id!= 1 
      parent=Category.find(parent_id)
      while parent.parent_id != 0 do # check, that main category.patent_is = 0!!!!
        all_parents <<  parent 
        parent=Category.find(parent.parent_id)      
      end      
    end
    all_parents
  end

  

  private

    def ensure_not_referenced_by_any_product
      if products.empty?
        return true
      else 
        errors.add(:base, "В категории имеются товары, удалите или переместите сначала их!")
        return false
      end
    end

    def ensure_not_referenced_by_any_category

      if subcategories.empty?
        return true
      else 
        errors.add(:base, "В категории имеются подкатегории, удалите или переместите сначала их!")
        return false
      end
    end

    def check_parent_id
      check_ids = get_all_subcats_ids() + [id]
       
      if check_ids.include?(parent_id)
        errors.add( :parent_id, "Категория не может быть вложена в саму себя!" )
        return false
      end



      return true
    end

    
end
