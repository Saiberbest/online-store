class Product < ActiveRecord::Base
  
  UNIT_TYPES = %w(мл. шт. г.)

  belongs_to :category
  has_many :line_items
  has_attached_file :image, :styles => { :medium => "200x200>", :thumb => "50x50>" }, :default_url => "/images/:style/missing.png"

  before_destroy :ensure_not_referenced_by_any_line_item


  validates_attachment :image,
    :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png"] }

  validates :title, :description, :price, :producer, :category, presence: true
  validates :price, :volume, numericality: {greater_than_or_equal_to: 0.01}
  validates :title, uniqueness: true
  #validates :units, inclusion: {in: UNIT_TYPES }
  
  
  def get_url
    "#{id}-#{producer.parameterize}-#{title.parameterize}"
  end



  private 

    def ensure_not_referenced_by_any_line_item
      if line_items.empty?
        return true
      else 
        errors.add(:base, "Существуют товарные позиции")
        return false
      end
    end
end
