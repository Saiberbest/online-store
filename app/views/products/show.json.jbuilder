json.extract! @product, :id, :title, :description, :image_url, :price, :volume, :category, :country, :producer, :created_at, :updated_at
