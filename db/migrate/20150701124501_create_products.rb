class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.string :description
      t.string :image_url
      t.decimal :price, precision: 8, scale: 2
      t.decimal :volume, precision: 8, scale: 2
      t.string :category
      t.string :country
      t.string :producer

      t.timestamps null: false
    end
  end
end
