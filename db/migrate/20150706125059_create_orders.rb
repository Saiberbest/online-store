class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.integer :index
      t.string :city
      t.string :street
      t.string :house
      t.string :email
      t.string :phone
      t.string :comment
      t.string :subsribe

      t.timestamps null: false
    end
  end
end
