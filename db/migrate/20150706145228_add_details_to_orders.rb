class AddDetailsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :apartment, :string
    change_column :orders, :comment, :text
  end
end
