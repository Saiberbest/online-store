class ChangeCategoryIdInProducts < ActiveRecord::Migration
  def change
    change_column :products, :category_id, :integer, default: 1
  end
end
