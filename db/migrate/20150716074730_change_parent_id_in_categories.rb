class ChangeParentIdInCategories < ActiveRecord::Migration
  def change
    change_column :categories, :parent_id, :integer, default: 1
  end
end
