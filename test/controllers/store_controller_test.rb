require 'test_helper'

class StoreControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "markup is in place" do
    get :index
    assert_select '.product.panel', 2
    assert_select '.product.panel input[type=submit]', 2
  end

end
