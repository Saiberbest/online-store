require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  test "product is not valid without a unique title" do
    product = Product.new(title: products(:shampoo).title,
      description: "yyy",
      price: 9.00,
      volume: 1,
      producer: "xxx",
      category: "Шампуни")

    assert product.invalid?

    assert_equal [I18n.translate('activerecord.errors.messages.taken')], product.errors[:title]
  end

end
